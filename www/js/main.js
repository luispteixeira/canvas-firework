var $status,
	$btnInit,
	$particlesNumber;

function draw(){	
	
  	FireworkOnCanvas.run();

	if(FireworkOnCanvas.clock/60*1000 < FireworkOnCanvas.fireworkTotalTime ) { 
		requestAnimationFrame(draw); 
		
	} else {
		//NO MORE PARTICLES TO RENDER SO NO REQUEST ANIMATION AND CLEAN THE STAGE
		FireworkOnCanvas.finish();
		
		$status.text('The fireworks ended, I hope you enjoyed the show.');
		$btnInit.text('Restart Firework').show().off('click');
		$btnInit.on('click',init);
	}
	
	$particlesNumber.text(FireworkOnCanvas.particles.length);	
}

function init(){

	$status.text('');
	$btnInit.hide();

	//Init Fireworks Object
	FireworkOnCanvas.init();	
	draw();
}


function xmlParser(xml){
	
	$status.text('Parsing Data...');

	
	if(typeof FireworkOnCanvas === 'object'){
		//Store Data on Fireworks Object
		FireworkOnCanvas.fireworkForDisplay = $.xml2json(xml);
		init();
	} else {
		$status.text('Missing Files');
		//alert("Something is wrong! Firework.js is needed, Can You Check it out!!");
	}
}


function loadXML(){

	$status.text('Loading...');
	
	$.get("xml/fireworks.xml", function(xml){
		$status.text('Config Data...');
		xmlParser(xml);		
	}).fail(function() {
    	alert("The XML File could not be processed correctly.");
  	});
}

$(document).ready(function(){
	
	$status = $('#status');
	$particlesNumber = $('.particle-number');
	$btnInit = $('.btn-init');
	$btnInit.on('click',loadXML);

});


