

(function() {
    var FireworkOnCanvas = {
        particlesRadius: 1,
        frameRate: 60,
        waitingFor: 3000,
        particles: [],
        firework: [],

        createParticles : function (xInit, yInit, colour , type, density){
            var particleCount = density ? density : 1;
            while( particleCount-- ) {
                this.particles.push(new Particle(xInit, yInit, colour, type));
            }
        },
        init: function(){
            this.canvas = document.getElementsByTagName('canvas')[0];
            this.canvas.width = document.body.clientWidth;
            this.canvas.height = document.body.clientHeight;

            this.ctx = this.canvas.getContext("2d");
            this.ctx.translate(this.canvas.width/2 , this.canvas.height/2 );
            this.ctx.fillStyle = 'rgba(0,0,0,.5)';

            this.particles = [];
            this.firework = [];
            
            this.clock = 0;
            this.then = Date.now();

            this.config();
        },

        config: function(){
            var self = this;

            $.each(this.fireworkForDisplay.Firework, function(i, obj){

                var positionX, positionY, velocityX, velocityY;

                ///TESTS
                var fireworkType = (obj.type !== undefined) ? obj.type.toLowerCase() : 'default';
                
                if(fireworkType === 'default' ) { console.log('Type of Firework is not Defined - The app can run without the desired appearance!'); }

                if(obj.Position !== undefined ){ 
                    positionX = Utils.isNumber(obj.Position.x) ? parseInt(obj.Position.x) : self.canvas.width/2;
                    positionY = Utils.isNumber(obj.Position.y) ? parseInt(obj.Position.y) : self.canvas.width/2;    
                } else {
                    positionX = parseInt(self.canvas.width/2);
                    positionY = parseInt(self.canvas.height/2);
                    console.log('Type of Firework is not Defined - The app can run without the desired appearance!');
                } 

                if(obj.Velocity !== undefined ){ 
                    velocityX = Utils.isNumber(obj.Velocity.x) ? parseInt(obj.Velocity.x) : self.canvas.width/2;
                    velocityY = Utils.isNumber(obj.Velocity.y) ? parseInt(obj.Velocity.y) : self.canvas.width/2;    
                } else {
                    velocityX = 150;
                    velocityY = parseInt(self.canvas.height/2);
                }


                //CREATE FIREWORK
                self.firework.push(
                    new Firework(

                        positionX,
                        positionY, 
                        obj.begin, 
                        obj.duration, 
                        obj.colour, 
                        fireworkType,
                        velocityX,
                        velocityY

                    ));

            });

            this.fireworkTotalTime = Math.max.apply(Math,this.firework.map(function(o){return o.beginAt; })) + this.waitingFor;
        },

        run: function(){
            
            this.clock ++;
            this.now = Date.now();
            this.delta = (this.now - this.then)/1000;
            this.then = this.now;
            
            this.ctx.clearRect(-this.canvas.width/2, -this.canvas.height/2, this.canvas.width, this.canvas.height);
        
            var i = this.firework.length;
            while( i-- ) {
                this.firework[ i ].draw();
                this.firework[ i ].update( i );
            }
        
            var ip = this.particles.length; 
            while( ip-- ) {
                this.particles[ ip ].draw();
                this.particles[ ip ].update( ip );
            }
        },

        finish : function() {
            this.firework = [];
            this.particles = [];
            this.ctx.clearRect(-this.canvas.width/2, -this.canvas.height/2, this.canvas.width, this.canvas.height);
        }
    };
    window.FireworkOnCanvas = FireworkOnCanvas;
})();



// shim layer with setTimeout fallback by Paul Irish
// Used as an efficient and browser-friendly
// replacement for setTimeout or setInterval
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame   ||
  window.mozRequestAnimationFrame      ||
  window.oRequestAnimationFrame        ||
  window.msRequestAnimationFrame       ||
  function (callback) {
    window.setTimeout(callback, 1000 / FireworkOnCanvas.frameRate);
  };
})();