(function() {
    var Utils = {
        math: {
            random: function(min, max){
                return Math.random() * ( max - min ) + min;
            }
        },
        
        colors: {
            hexToR : function(h){
                return parseInt((this.cutHex(h)).substring(0,2),16);
            },
            hexToG : function(h){
                return parseInt((this.cutHex(h)).substring(2,4),16);
            },
            hexToB : function(h){
                return parseInt((this.cutHex(h)).substring(4,6),16);
            },
            cutHex : function(h){
                return (h.charAt(0)==="#") ? h.substring(1,7):h;
            }
        },

        isNumber : function (n){
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    };
    window.Utils = Utils;
})();
