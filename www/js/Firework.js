if(typeof FireworkOnCanvas !== "object" ){
	alert("Something is wrong! FireworkOnCanvas.js is needed, Can You Check it out!!");
}
function Firework(xInit ,yInit, beginAt, lifeTime, colour, type , velocityX, velocityY) {
	
	// actual coordinates
	this.x = xInit;
	this.y = yInit;
	// starting coordinates
	this.xInit = xInit;
	this.yInit = yInit;
	
	//star after 
	this.beginAt = parseInt(beginAt);
	//Fireworks lifetime
	this.lifeTime = parseInt(lifeTime);
	//fireworks colour
	this.colour = colour;
	this.hexColor = '#'+parseInt(colour).toString(16);

	//fireworks type (rocket, fountain)
	this.type = type;

	if(this.type === 'rocket') {
		this.created = false;	
		this.velocityX = velocityX;
		this.velocityY = velocityY/2;
		this.y = -yInit;

		//create a trail effect
		this.coordinates = [];
		this.coordinateCount = 5;
		// populate coordinate with the current pos x and pos y
		while( this.coordinateCount-- ) {
			this.coordinates.push( [ this.x, this.y ] );
		}
	} 
	//console.log(this.xVel, this.yVel);
}

Firework.prototype.update = function(i) {
	
	if(this.type === 'rocket'){

        var velocityY = this.velocityY * FireworkOnCanvas.delta;
        var velocityX = this.velocityX * FireworkOnCanvas.delta;
		
		if(FireworkOnCanvas.clock/60*1000 > this.beginAt && !this.created){
			this.x = this.x - velocityX;
        	this.y = this.y - velocityY;
        	// remove last item in coordinates array
			this.coordinates.pop();
			// add current coordinates to the start of the array
			this.coordinates.unshift( [ this.x, this.y ] );

		} 
		//END OF LIFE ROCKET
		if(FireworkOnCanvas.clock/60*1000 > this.beginAt + this.lifeTime && !this.created){
			FireworkOnCanvas.createParticles(this.x, this.y, this.colour, this.type, 150);
			FireworkOnCanvas.firework.splice( i, 1 );		
		}

	} else {

		if(FireworkOnCanvas.clock/60*1000 < this.lifeTime){
			FireworkOnCanvas.createParticles(this.x, -this.y, this.colour, this.type);
		}

		//END OF LIFE DEFAULT FIREWORK
		if(FireworkOnCanvas.clock/60*1000 > this.beginAt + this.lifeTime + FireworkOnCanvas.waitingFor){ 
			FireworkOnCanvas.firework.splice( i, 1 );
		}
	}
};

Firework.prototype.draw = function() {

    if(this.type === 'rocket'){
	    FireworkOnCanvas.ctx.beginPath();
		// move to the last tracked coordinate in the set, then draw a line to the current x and y
		FireworkOnCanvas.ctx.moveTo( this.coordinates[ this.coordinates.length - 1][ 0 ], this.coordinates[ this.coordinates.length - 1][ 1 ] );
		FireworkOnCanvas.ctx.lineTo( this.x, this.y );
		FireworkOnCanvas.ctx.strokeStyle = 'rgba('+Utils.colors.hexToR(this.hexColor)+','+Utils.colors.hexToG(this.hexColor)+','+Utils.colors.hexToB(this.hexColor)+',.5)';
		FireworkOnCanvas.ctx.stroke();

	    FireworkOnCanvas.ctx.beginPath();
		FireworkOnCanvas.ctx.arc(this.x,this.y,FireworkOnCanvas.particlesRadius,0,2*Math.PI);
		FireworkOnCanvas.ctx.fillStyle = 'rgba('+Utils.colors.hexToR(this.hexColor)+','+Utils.colors.hexToG(this.hexColor)+','+Utils.colors.hexToB(this.hexColor)+',1)';//this.colour;//'rgba(255, 0, 0, '+this.alpha+')';
		FireworkOnCanvas.ctx.fill();
    } 

};
