if(typeof Firework !== "function" ){
	alert("Something is wrong! Firework.js is needed, Can You Check it out!!");
}

function Particle(x,y,colour,type){
	
	this.x = x;
	this.y = y;
	this.alpha = 1;
	this.colour = '#'+parseInt(colour).toString(16);
	this.type = type;

	if(this.type === "rocket"){
		
		//particleSound.play();
		this.angle = Utils.math.random( 0, Math.PI * 2 );
		this.speed = Utils.math.random( 1, 10 );
		this.decay = Utils.math.random( 0.007, 0.01 );
		//friction will slow the particle down
		this.friction = 0.95;
		//gravity will be applied and pull the particle down
		this.gravity = 1;

	} else {

		this.decay = Utils.math.random( 0.004, 0.007 );
		this.fountainY = Utils.math.random(-55,-60);
		this.fountainX = Utils.math.random(-10,10);
	}
	
}

Particle.prototype.update = function (i){
	//console.log('upade');
	
	
	if(this.type === "rocket") {
		this.speed *= this.friction;
		// apply velocity
		this.x += Math.cos( this.angle ) * this.speed;
		this.y += Math.sin( this.angle ) * this.speed + this.gravity;
	} else {
		//fountain
		this.x += this.fountainX * 0.08;
		this.y += this.fountainY * 0.08;
		this.fountainY += 0.8;//this.gravity ;
	 	//
	}

	// fade out the particle
	this.alpha -= this.decay;
	
	// remove the particle once the alpha is low enough
	if( this.alpha <= this.decay ) {
		FireworkOnCanvas.particles.splice( i, 1 );
		
	}
};

Particle.prototype.draw = function (){
	
	FireworkOnCanvas.ctx.beginPath();
	FireworkOnCanvas.ctx.arc(this.x,this.y,FireworkOnCanvas.particlesRadius,0,2*Math.PI);
	FireworkOnCanvas.ctx.fillStyle = 'rgba('+Utils.colors.hexToR(this.colour)+','+Utils.colors.hexToG(this.colour)+','+Utils.colors.hexToB(this.colour)+','+this.alpha+')';//this.colour;//'rgba(255, 0, 0, '+this.alpha+')';
	FireworkOnCanvas.ctx.fill();

};