
var appConfig = {
	particlesRadius: 1,
    frameRate: 60,
    waitingFor: 3000,
};

var $status,
	$btnInit,
	$particlesNumber;

var FireworkDisplay,
	canvas,
	ctx,
	particles,
	firework,
	clock,
	fireworkTotalTime,
	then,
	now,
	delta;

// create particle group/explosion
function createParticles( xInit, yInit, colour , type, density) {
	
	var particleCount = density ? density : 1;
	while( particleCount-- ) {
		particles.push( new Particle(  xInit, yInit, colour, type));
	}
}


function Firework(xInit ,yInit, beginAt, lifeTime, colour, type , velocityX, velocityY) {
	
	// actual coordinates
	this.x = xInit;
	this.y = yInit;
	// starting coordinates
	this.xInit = xInit;
	this.yInit = yInit;
	
	//star after 
	this.beginAt = parseInt(beginAt);
	//Fireworks lifetime
	this.lifeTime = parseInt(lifeTime);
	//fireworks colour
	this.colour = colour;
	this.hexColor = '#'+parseInt(colour).toString(16);

	//fireworks type (rocket, fountain)
	this.type = type;

	if(this.type === 'rocket') {
		this.created = false;	
		this.velocityX = velocityX;
		this.velocityY = velocityY/2;
		this.y = -yInit;

		//create a trail effect
		this.coordinates = [];
		this.coordinateCount = 5;
		// populate coordinate with the current pos x and pos y
		while( this.coordinateCount-- ) {
			this.coordinates.push( [ this.x, this.y ] );
		}
	} 
	//console.log(this.xVel, this.yVel);
}

Firework.prototype.update = function(i) {
	
	if(this.type === 'rocket'){

        var velocityY = this.velocityY * delta;
        var velocityX = this.velocityX * delta;
		
		if(clock/60*1000 > this.beginAt && !this.created){
			this.x = this.x - velocityX;
        	this.y = this.y - velocityY;
        	// remove last item in coordinates array
			this.coordinates.pop();
			// add current coordinates to the start of the array
			this.coordinates.unshift( [ this.x, this.y ] );

		} 
		//END OF LIFE ROCKET
		if(clock/60*1000 > this.beginAt + this.lifeTime && !this.created){
			createParticles(this.x, this.y, this.colour, this.type, 150);
			firework.splice( i, 1 );		
		}

	} else {

		if(clock/60*1000 < this.lifeTime){
			createParticles(this.x, -this.y, this.colour, this.type);
		}

		//END OF LIFE DEFAULT FIREWORK
		if(clock/60*1000 > this.beginAt + this.lifeTime + appConfig.waitingFor){ 
			firework.splice( i, 1 );
		}
	}
};

Firework.prototype.draw = function() {

    if(this.type === 'rocket'){
	    ctx.beginPath();
		// move to the last tracked coordinate in the set, then draw a line to the current x and y
		ctx.moveTo( this.coordinates[ this.coordinates.length - 1][ 0 ], this.coordinates[ this.coordinates.length - 1][ 1 ] );
		ctx.lineTo( this.x, this.y );
		ctx.strokeStyle = 'rgba('+Utils.colors.hexToR(this.hexColor)+','+Utils.colors.hexToG(this.hexColor)+','+Utils.colors.hexToB(this.hexColor)+',.5)';
		ctx.stroke();

	    ctx.beginPath();
		ctx.arc(this.x,this.y,appConfig.particlesRadius,0,2*Math.PI);
		ctx.fillStyle = 'rgba('+Utils.colors.hexToR(this.hexColor)+','+Utils.colors.hexToG(this.hexColor)+','+Utils.colors.hexToB(this.hexColor)+',1)';//this.colour;//'rgba(255, 0, 0, '+this.alpha+')';
		ctx.fill();
    } 

};

function Particle(x,y,colour,type){
	
	this.x = x;
	this.y = y;
	this.alpha = 1;
	this.colour = '#'+parseInt(colour).toString(16);
	this.type = type;

	if(this.type === "rocket"){
		
		//particleSound.play();
		this.angle = Utils.math.random( 0, Math.PI * 2 );
		this.speed = Utils.math.random( 1, 10 );
		this.decay = Utils.math.random( 0.007, 0.01 );
		//friction will slow the particle down
		this.friction = 0.95;
		//gravity will be applied and pull the particle down
		this.gravity = 1;

	} else {

		this.decay = Utils.math.random( 0.004, 0.007 );
		this.fountainY = Utils.math.random(-55,-60);
		this.fountainX = Utils.math.random(-10,10);
	}
	
}

Particle.prototype.update = function (i){
	//console.log('upade');
	
	
	if(this.type === "rocket") {
		this.speed *= this.friction;
		// apply velocity
		this.x += Math.cos( this.angle ) * this.speed;
		this.y += Math.sin( this.angle ) * this.speed + this.gravity;
	} else {
		//fountain
		this.x += this.fountainX * 0.08;
		this.y += this.fountainY * 0.08;
		this.fountainY += 0.8;//this.gravity ;
	 	//
	}

	// fade out the particle
	this.alpha -= this.decay;
	
	// remove the particle once the alpha is low enough
	if( this.alpha <= this.decay ) {
		particles.splice( i, 1 );
		
	}
};

Particle.prototype.draw = function (){
	
	ctx.beginPath();
	ctx.arc(this.x,this.y,appConfig.particlesRadius,0,2*Math.PI);
	ctx.fillStyle = 'rgba('+Utils.colors.hexToR(this.colour)+','+Utils.colors.hexToG(this.colour)+','+Utils.colors.hexToB(this.colour)+','+this.alpha+')';//this.colour;//'rgba(255, 0, 0, '+this.alpha+')';
	ctx.fill();

};


// shim layer with setTimeout fallback by Paul Irish
// Used as an efficient and browser-friendly
// replacement for setTimeout or setInterval
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame   ||
  window.mozRequestAnimationFrame      ||
  window.oRequestAnimationFrame        ||
  window.msRequestAnimationFrame       ||
  function (callback) {
    window.setTimeout(callback, 1000 / appConfig.frameRate);
  };
})();


function init(){

	$status.text('');
	$btnInit.hide();

	canvas = document.getElementsByTagName('canvas')[0];
	canvas.width = document.body.clientWidth;
	canvas.height = document.body.clientHeight;

	ctx = canvas.getContext("2d");
	ctx.translate(canvas.width/2 , canvas.height/2 );
	ctx.fillStyle = 'rgba(0,0,0,.5)';

	particles = [];
	firework = [];
	
	clock = 0;
	then = Date.now();

	$.each(FireworkDisplay.Firework, function(i, obj){

		var positionX, positionY, velocityX, velocityY;

		///TESTS
		var fireworkType = (obj.type !== undefined) ? obj.type.toLowerCase() : 'default';
		
		if(fireworkType === 'default' ) { console.log('Type of Firework is not Defined - The app can run without the desired appearance!'); }

		if(obj.Position !== undefined ){ 
			positionX = Utils.isNumber(obj.Position.x) ? parseInt(obj.Position.x) : canvas.width/2;
			positionY = Utils.isNumber(obj.Position.y) ? parseInt(obj.Position.y) : canvas.width/2;	
		} else {
			positionX = parseInt(canvas.width/2);
			positionY = parseInt(canvas.height/2);
			console.log('Type of Firework is not Defined - The app can run without the desired appearance!');
		} 

		if(obj.Velocity !== undefined ){ 
			velocityX = Utils.isNumber(obj.Velocity.x) ? parseInt(obj.Velocity.x) : canvas.width/2;
			velocityY = Utils.isNumber(obj.Velocity.y) ? parseInt(obj.Velocity.y) : canvas.width/2;	
		} else {
			velocityX = 150;
			velocityY = parseInt(canvas.height/2);
		}


		//CREATE FIREWORK
		firework.push(new Firework(

					positionX,
					positionY, 
					obj.begin, 
					obj.duration, 
					obj.colour, 
					fireworkType,
					velocityX,
					velocityY

				));
	});

	fireworkTotalTime = Math.max.apply(Math,firework.map(function(o){return o.beginAt; })) + appConfig.waitingFor;
	
	draw();
  	
	function draw(){	
	  	
		//CLOCKS
	  	clock ++;
	  	now = Date.now();
	  	delta = (now - then)/1000;
	  	then = now;
	  	
	  	//erase canvas
	  	ctx.clearRect(-canvas.width/2, -canvas.height/2, canvas.width, canvas.height);
	  	
	  	var i = firework.length;
		while( i-- ) {
			firework[ i ].draw();
			firework[ i ].update( i );
		}
		
		var ip = particles.length;	
		while( ip-- ) {
			particles[ ip ].draw();
			particles[ ip ].update( ip );
		}

		if(clock/60*1000 < fireworkTotalTime ) { 
			requestAnimationFrame(draw); 
			
		} else {
			firework = [];
			particles = [];
			ctx.clearRect(-canvas.width/2, -canvas.height/2, canvas.width, canvas.height);
			$status.text('The fireworks ended, I hope you enjoyed the show.');
			$btnInit.text('Restart Firework').show().off('click');
			$btnInit.on('click',init);
		}
		
		$particlesNumber.text(particles.length);	
	}
}

function xmlParser(xml){
	
	$status.text('Parsing Data...');
	FireworkDisplay = $.xml2json(xml);
	init();
}


function loadXML(){

	$status.text('Loading...');
	
	$.get("xml/fireworks.xml", function(xml){
		$status.text('Config Data...');
		xmlParser(xml);		
	}).fail(function() {
    	alert("The XML File could not be processed correctly.");
  	});
}

$(document).ready(function(){
	
	$status = $('#status');
	$particlesNumber = $('.particle-number');
	$btnInit = $('.btn-init');
	$btnInit.on('click',loadXML);

});


