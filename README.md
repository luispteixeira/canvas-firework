# README #

Firework on Canvas - Exercise with HTML and Javascript and Canvas

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Grunt.js](http://gruntjs.com/) 

## Installation

* `git clone git@bitbucket.org:luispteixeira/canvas-firework.git` this repository
* change into the new directory
* npm install 

## Running 

* `grunt serve`
* Visit your app at [http://localhost:9000](http://localhost:9000).


## OR you can see it here

[DEMO](http://fireworks-on-canvas.mynameisluis.com/)